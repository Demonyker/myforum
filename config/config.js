require('dotenv').config();

module.exports = {
  development: {
    use_env_variable: 'DEVELOPMENT_DATABASE_URL',
    username: 'coeudgmbprknlp',
    password: 'baec098804b50143ce548da2a158571ba48dfcde0cfcbb35205f3853afd5a84b',
    database: 'd1sg6276b4hta8',
    host: 'ec2-54-246-87-132.eu-west-1.compute.amazonaws.com',
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
  stage: {
    use_env_variable: 'STAGE_DATABASE_URL',
    username: 'zonjdmyqppadyt',
    password: '1ad23ebefb6429b0abb4cb00e61435f24fa7b88ac69a3d9e7bee969c3214b359',
    database: 'dcoa75g2a7s45u',
    host: 'ec2-52-212-157-46.eu-west-1.compute.amazonaws.com',
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
  production: {
    use_env_variable: 'PRODUCTION_DATABASE_URL',
    username: 'vadjakmlhaanit',
    password: '168b74746cf8540536b1cd44c8eda1c1c056f9537e2713d62813d27f8ccdbb4e',
    database: 'dbc29sgc9l549s',
    host: 'ec2-54-246-87-132.eu-west-1.compute.amazonaws.com',
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
