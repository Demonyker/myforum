const { app } = require('./config');
const {
  authenticationRoter,
} = require('./api');
const { handleErrors } = require('./helpers');

app.set('PORT', process.env.PORT || 3000);
app.use('/api', authenticationRoter);
app.use(handleErrors);
app.listen(app.get('PORT'), () => console.log(`Example app listening at http://localhost:${app.get('PORT')}`));
