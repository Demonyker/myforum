const { UserService } = require('./user.js');
const { AuthenticationService } = require('./authentication.js');

module.exports = {
  UserService,
  AuthenticationService,
};
