const { signUpSchema, signInSchema } = require('./authentication');

module.exports = {
  signUpSchema,
  signInSchema,
};
